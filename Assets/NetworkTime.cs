﻿//
// NetworkTime - helper class for UNET server time synchronization
//
// Copyright (C) 2018 Boris Novikov @ dnnkeeper
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;


public class CustomMsgType
{
    public const short SyncTimeMsgType = MsgType.Highest + 1;
}

public class SyncTimeMessage : MessageBase
{
    public int timestamp;
    public double Time;
}

[RequireComponent(typeof(NetworkManager))]
public class NetworkTime : MonoBehaviour
{
    public static NetworkTime singleton;
    
    //time offset of this client relative to the server. Always 0 on server itself
    public static double serverTimeOffset;

    //if above zero - this amount of sync messages will be sent when new client connects with 0.1f delay
    public int syncMessageCount = 0;

    //if above zero - will be sending sync messages every period of time
    public float syncPeriod = 2.0f;

    //blinks with main camera background color if enabled and shows GUI
    public bool debug;

    //last missmatch between expected servertime and servertime in arrived message
    float expectedTimeMissmatch;

    //avearge error
    float expectedTimeMissmatchAvg;

    //last delay of arrived message in MS
    float lastTimeDelayMS;

    bool init;

    bool isClientActive;

    bool isServerActive;

    //keeps track of all network connections passed initial synchronization
    HashSet<NetworkConnection> syncedConnections = new HashSet<NetworkConnection>(); 

    public static double ServerTime
    {
        get
        {
            return Time.realtimeSinceStartup + serverTimeOffset;
        }
    }

    void OnEnable()
    {
        SceneManager.sceneLoaded += OnLevelFinishedLoading;
    }

    void OnDisable()
    {
        SceneManager.sceneLoaded -= OnLevelFinishedLoading;
    }

    void Awake()
    {
        if (singleton == null)
        {
            if (transform.parent == null)
                DontDestroyOnLoad(this);
            singleton = this;
        }
        else
        {
            Destroy(this);
        }
    }

    void Update()
    {
        isClientActive = NetworkClient.active;

        isServerActive = NetworkServer.active;

        if (isClientActive || isServerActive)
        {
            if (!init)
            {
                RegisterHandler();
                init = true;
            }
        }
        else
        {
            init = false;
        }

        //Start time synchronization for every new connection
        foreach (NetworkConnection connectionToClient in NetworkServer.connections)
        {
            if (connectionToClient != null)
            {
                if (connectionToClient.isConnected && !syncedConnections.Contains(connectionToClient))
                {
                    if (syncPeriod > 0f && syncMessageCount == 0f )
                    {
                        StartCoroutine(SyncTimeRoutine(connectionToClient, syncPeriod, x => { syncedConnections.Remove(connectionToClient); }));
                    }
                    else
                    {
                        if (syncMessageCount > 0)
                        {
                            StartCoroutine(SendTimeSync(connectionToClient, 0.1f, syncMessageCount));
                        }
                        else
                            SendTimeSync(connectionToClient);
                    }
                        
                    syncedConnections.Add(connectionToClient);
                }
            }
        }
            
        //clear invalid connections
        syncedConnections.RemoveWhere(conn => conn.address.Length == 0);

        //blink
        if (debug)
        {
            if ((ServerTime % 1f) <= 1f/30f)
            {
                Camera.main.backgroundColor = Color.grey;
            }
            else
            {
                Camera.main.backgroundColor = Color.black;
            }
        }
    }

    void RegisterHandler()
    {
        if (isServerActive)
        {
            if (debug)
                Debug.Log("Register ServerTimeRequest handler");
            NetworkServer.RegisterHandler(CustomMsgType.SyncTimeMsgType, ServerTimeRequest);
        }
        else if (isClientActive)
        {
            if (debug)
                Debug.Log("Register OnReceiveSyncTime handler");
            NetworkClient.allClients[0].RegisterHandler(CustomMsgType.SyncTimeMsgType, SyncTimeRequest);
        }
    }

    void ServerTimeRequest(NetworkMessage msg)
    {
        if (debug)
            Debug.Log("Time request from "+msg.conn.address);
        SendTimeSync(msg.conn);
    }
    
    

    void SyncTimeRequest(NetworkMessage msg)
    {
        var syncTimeMessage = msg.ReadMessage<SyncTimeMessage>();

        byte error;
        lastTimeDelayMS = NetworkTransport.GetRemoteDelayTimeMS(msg.conn.hostId, msg.conn.connectionId, syncTimeMessage.timestamp, out error);

        //how much did we miss this time
        expectedTimeMissmatch = (float)(ServerTime - syncTimeMessage.Time);

        //check if it was first sync so we don't interpolate and set it later
        if (expectedTimeMissmatch > 0.1f)
            expectedTimeMissmatchAvg = 0f;
        else
            expectedTimeMissmatchAvg = Mathf.Lerp(expectedTimeMissmatchAvg, expectedTimeMissmatch, 0.1f); //error averaging

        float delay = lastTimeDelayMS * 0.001f;
        
        //Reported server time is late by delay and probably less than that by (time wasted on local frame update + 1/serverFPS) and by average error
        double predictedServerTime = syncTimeMessage.Time + delay - (Time.deltaTime + 1/60f) - expectedTimeMissmatchAvg;
        
        float newTimeOffset = (float)(predictedServerTime - Time.realtimeSinceStartup);
            
        //Debug.Log("syncTimeMessage.Time = " + syncTimeMessage.Time.ToString("F6") + "; delay = " + delay.ToString("F6"));

        //Debug.Log("Local Time = " + Time.realtimeSinceStartup.ToString("F6"));

        //Debug.Log("timeOffset = "+timeOffset +" + " + (newTimeOffset-timeOffset).ToString("F6")+" = "+ newTimeOffset.ToString("F6"));

        serverTimeOffset = newTimeOffset;
        
        if (debug)
            Debug.Log("ServerTime = " + ServerTime.ToString("F6") + "; delay = " + (delay).ToString("F3")+";  ");
    }

    public void SendTimeSyncToAll()
    {
        //Debug.Log("SYNC");

        SyncTimeMessage msg = new SyncTimeMessage();

        msg.timestamp = NetworkTransport.GetNetworkTimestamp();

        msg.Time = Time.realtimeSinceStartup;

        foreach (NetworkConnection connectionToClient in NetworkServer.connections)
        {
            if (connectionToClient != null)
                connectionToClient.Send(CustomMsgType.SyncTimeMsgType, msg);
        }
    }

    public void SendTimeSync(NetworkConnection connectionToClient)
    {
        SyncTimeMessage msg = new SyncTimeMessage();

        msg.timestamp = NetworkTransport.GetNetworkTimestamp();

        msg.Time = Time.realtimeSinceStartup;
            
        connectionToClient.Send(CustomMsgType.SyncTimeMsgType, msg);
    }

    public void RequestServerTime()
    {
        if (debug)
            Debug.Log("RequestServerTime");

        SyncTimeMessage msg = new SyncTimeMessage();

        msg.timestamp = NetworkTransport.GetNetworkTimestamp();

        msg.Time = Time.realtimeSinceStartup;

        NetworkClient.allClients[0].connection.Send(CustomMsgType.SyncTimeMsgType, msg);
    }

    public IEnumerator SendTimeSync(NetworkConnection connectionToClient, float delay, int count)
    {
        for (int i = count; i > 0; i--)
        {
            SendTimeSync(connectionToClient);
            yield return new WaitForSecondsRealtime(delay);
        }
    }

    public IEnumerator syncTimeAllRoutine()
    {
        while (NetworkServer.active)
        {
            SendTimeSyncToAll();
            yield return new WaitForSecondsRealtime(syncPeriod);
        }
    }

    public IEnumerator SyncTimeRoutine(NetworkConnection connectionToClient, float delay, System.Action<bool> Callback = null)
    {
        while (connectionToClient != null && connectionToClient.address.Length > 0)
        {
            SendTimeSync(connectionToClient);

            yield return new WaitForSecondsRealtime(delay);
        }
        if (Callback != null)
            Callback(true);
    }

    private void OnConnectedToServer()
    {
        if (debug)
            Debug.Log("OnConnectedToServer!");
        RequestServerTime();
    }

    void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
    {
        if (isClientActive)
        {
            RequestServerTime();
        }
    }

    public float GetLastTimeDelay()
    {
        return lastTimeDelayMS;
    }

    private void OnGUI()
    {
        if (!debug)
            return;

        GUILayout.Space(200);

        GUILayout.Label("CorrectedServerTime = " + ServerTime.ToString("F6")+"s");

        if (NetworkClient.active)
        {
            GUILayout.Label("lastTimeDelay = " + lastTimeDelayMS+"ms");

            GUILayout.Label("expectedTimeMissmatch = " + (expectedTimeMissmatch).ToString("F6"));

            if (GUILayout.Button("Request Server Time"))
            {
                RequestServerTime();
            }

        }
        else if (NetworkServer.active)
        {
            if (GUILayout.Button("Sync Time"))
            {
                SendTimeSyncToAll();
            }

            GUILayout.Label("NetworkServer.connections:");


            foreach (NetworkConnection c in NetworkServer.connections)
            {
                if (c == null)
                    GUILayout.Label("Null connection");
                else
                    GUILayout.Label(c.address);
            }
            
            GUILayout.Space(20);

            GUILayout.Label("syncedConnections:");
            
            foreach (NetworkConnection c in syncedConnections)
            {
                if (c == null)
                    GUILayout.Label("Null connection");
                else
                {
                    GUILayout.Label(c.address + " " + c.isConnected + " " + c.connectionId);
                }
            }
            
        }
    }
}