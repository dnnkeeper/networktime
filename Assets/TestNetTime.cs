﻿using UnityEngine;
using UnityEngine.Networking;

public class TestNetTime : MonoBehaviour
{
    private void OnGUI()
    {
        if (NetworkTime.singleton == null || NetworkTime.singleton.debug)
            return;
        
        GUILayout.Space(200);

        GUILayout.Label("CorrectedServerTime = " + NetworkTime.ServerTime.ToString("F6")+"s");
        
        if (NetworkClient.active)
        {
            GUILayout.Label("LastTimeDelay (PING/2) = " + NetworkTime.singleton.GetLastTimeDelay().ToString("F3")+"ms");

            if (GUILayout.Button("Request Server Time"))
            {
                NetworkTime.singleton.RequestServerTime();
            }
        }
        else if (NetworkServer.active)
        {
            if (GUILayout.Button("Sync Time"))
            {
                NetworkTime.singleton.SendTimeSyncToAll();
            }
        }
    }
}
