# README #

Forum thread:
https://forum.unity.com/threads/network-time-in-2017-3-workaround.523462/

Due to old RakNet unity network being removed in new versions I run into need of a Network.Time replacement so I can continue to use reliable timestamps for network states interpolation.
Long story short: I wrote a script sending Time.realtimeSinceStartup form server to clients and managed to correct network delay using NetworkTransport.GetRemoteDelayTimeMS and substracting some predicted average error.

My estimation is following:
Reported server time is late by delay measured with NetworkTransport.GetRemoteDelayTimeMS and probably less than that by (time wasted on local frame update + 1/serverFPS) and by average error

So
double predictedServerTime = syncTimeMessage.Time + delay - (Time.deltaTime + 1/60f) - expectedTimeMissmatchAvg;

To get it working just add this component to your NetworkManager game object